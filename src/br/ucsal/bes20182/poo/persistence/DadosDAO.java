package br.ucsal.bes20182.poo.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.poo.domain.Dados;

public class DadosDAO {

	public List<Dados> dados = new ArrayList<>();

	public String incluir(Dados dado) {
		dados.add(dado);
		return null;
	}

	public List<Dados> obterTodos() {
		return dados;
	}	
}
