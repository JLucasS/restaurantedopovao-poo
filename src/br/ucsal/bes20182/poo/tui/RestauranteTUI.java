package br.ucsal.bes20182.poo.tui;

import java.util.List;
import java.util.Scanner;

import br.ucsal.bes20182.poo.business.DadosBO;
import br.ucsal.bes20182.poo.domain.Dados;
import br.ucsal.bes20182.poo.domain.Estado;

public class RestauranteTUI {
	
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		Integer cont;
		System.out.println("Informe a quantidade de clientes no restaurante");
		cont = sc.nextInt();
		sc.nextLine();
		
		RestauranteTUI restaurante = new RestauranteTUI();
		
		for (int i = 0; i < cont; i++) {
			restaurante.registrar();
			restaurante.mostar();
		}

		
	}
	

	public DadosBO dadosBO = new DadosBO();
	
	public void registrar() {
		System.out.println("BEM VINDO A INCLUS�O DE CLIENTES DO RESTAURANTE DO POV�O:");
		String codigo = obterTexto("Informe o codigo do cliente: ");
		String nome = obterTexto("Informe o nome do cliente: ");
		Integer valorUnit = obterInteiro("Informe a quantidade de pessoas na mesa ");
		Estado estado = obterEstado();

		Dados dado = new Dados(codigo, nome, valorUnit, estado);

		String erro = DadosBO.incluir(dado);
		if (erro != null) {
			System.out.println("Falha ao incluir o cliente!");
			System.out.println(erro);
		}

	}

	public void mostar() {
		System.out.println("A situa��o atual no restaurante �: ");
		List<Dados> dados = dadosBO.obterTodos();
		for (int i = 0; i < dados.size(); i++) {
			System.out.println(" \tCodigo do cliente: " + dados.get(i).getCodigo());
			System.out.println(" \tNome do cliente: " + dados.get(i).getNome());
			System.out.println(" \tValor a ser pago: " + dados.get(i).getValorUnit());
			System.out.println();
		}
	}

	private Estado obterEstado() {
		while (true) {
			System.out.println("O estado atual da mesa � (" + Estado.obterEstado() + "):");
			String estadoString = sc.nextLine();

			try {
				Estado estado = Estado.valueOf(estadoString.toUpperCase());
				return estado;
			} catch (IllegalArgumentException e) {
				System.out.println("O estado informado n�o � valido!");
			}
		}

	}

	private Integer obterInteiro(String mensagem) {
		System.out.println(mensagem);
		Integer valor = sc.nextInt();
		sc.nextLine();
		return valor;
	}

	private String obterTexto(String mensagem) {
		System.out.println(mensagem);
		return sc.nextLine();
	}

}
