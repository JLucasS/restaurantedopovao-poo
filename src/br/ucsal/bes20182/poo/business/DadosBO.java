package br.ucsal.bes20182.poo.business;

import java.util.List;

import br.ucsal.bes20182.poo.domain.Dados;
import br.ucsal.bes20182.poo.persistence.DadosDAO;

public class DadosBO {
	
	public static DadosDAO dadosDAO = new DadosDAO();
	
	public static String incluir(Dados dado) {
		String erro = validar(dado);
		
		if (erro != null) {
			return erro;
		}
		return dadosDAO.incluir(dado);
	}
	public List<Dados> obterTodos() {
		return dadosDAO.obterTodos();
	}
	
	private static String validar(Dados dado) {
		if (dado.getCodigo().trim().isEmpty()) {
			return "ERRO: C�digo do cliente n�o informado!";
		}
		if (dado.getNome().trim().isEmpty()) {
			return "ERRO: Nome do Cliente n�o informado!";
		}
		if (dado.getValorUnit().toString().trim().isEmpty()) {
			return "ERRO: Valor a ser paago n�o informado!";
		}
		return null;
	}

}
