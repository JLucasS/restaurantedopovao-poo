package br.ucsal.bes20182.poo.domain;

public class Dados {

	private String codigo;

	private String nome;

	private Integer valorUnit;

	private Estado estado;

	public Dados(String codigo, String nome, Integer valorUnit, Estado estado) {
		this.codigo = codigo;
		this.nome = nome;
		this.valorUnit = valorUnit;
		this.estado = estado;

	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getValorUnit() {
		return valorUnit;
	}

	public void setValorUnit(Integer valorUnit) {
		this.valorUnit = valorUnit;
	}

}
