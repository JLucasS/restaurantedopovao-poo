package br.ucsal.bes20182.poo.domain;

public enum Estado {
	LIBERADA, MANUTENÇÃO, OCUPADA;

	public static String obterEstado() {
		String estadoMesa = "";
		for (Estado estado : values()) {
			estadoMesa += estado + ",";
		}
		return estadoMesa.substring(0, estadoMesa.length() - 1);
	}

}
